using UnityEngine;
using Zenject;

namespace Sandbox.ModuleA
{
    public class ModuleAInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<LoadingServices>().AsSingle().NonLazy();
            Container.Bind<CubeSpawner>().AsSingle().NonLazy();
        }
    }
}