using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Sandbox.ModuleA
{
    public class CubeSpawner
    {
        private LoadingServices _loadingServices;

        private GameObject _prefab;

        public CubeSpawner(LoadingServices loadingServices)
        {
            _loadingServices = loadingServices;
            LoadResources();
            Update();
        }

        private async void LoadResources()
        {
            _prefab = await _loadingServices.LoadByKey<GameObject>("BaseCube");
        }

        private async void Update()
        {
            while (Application.isPlaying)
            {
                if (Input.GetKeyDown(KeyCode.A))
                {
                    Spawn();
                }

                await Task.Yield();
            }     
        }

        private void Spawn()
        {
            Debug.Log($"Spawn A {_prefab}");
            if (_prefab != null)
            {
                Object.Instantiate(_prefab, Vector3.left * 5, Quaternion.identity);
            }
        }
    }
}
