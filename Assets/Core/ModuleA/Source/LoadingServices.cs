using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.AddressableAssets.ResourceLocators;

namespace Sandbox.ModuleA
{
    public class LoadingServices
    {
        public LoadingServices()
        {
        }

        public async Task<TObject> LoadByKey<TObject>(object key)
        {
            var handler = Addressables.LoadAssetAsync<TObject>(key);
            var result = await handler.Task;
            //Addressables.Release(handler);
            return result;
        }
    }
}
