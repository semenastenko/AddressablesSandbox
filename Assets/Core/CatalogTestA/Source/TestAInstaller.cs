using UnityEngine;
using Zenject;

namespace Sandbox.CatalogTestA
{
    public class TestAInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<InfoPrinter>().AsSingle().NonLazy();
        }
    }
}