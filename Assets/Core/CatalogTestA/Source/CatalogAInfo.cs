using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sandbox.CatalogTestA
{
    [CreateAssetMenu(fileName = "CatalogAInfo", menuName = "SO/CatalogAInfo")]
    public class CatalogAInfo : ScriptableObject
    {
        [field: SerializeField]
        public string Value { get; set; }
    }
}
