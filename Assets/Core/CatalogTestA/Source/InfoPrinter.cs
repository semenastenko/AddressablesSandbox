﻿using Sandbox.ModuleA;
using UnityEngine;
using UnityEngine.UI;

namespace Sandbox.CatalogTestA
{
    public class InfoPrinter
    {
        private LoadingServices _loadingServices;
        private Text _textField;
        private CatalogAInfo _info;
        public InfoPrinter(LoadingServices loadingServices, Text textField)
        {
            _loadingServices = loadingServices;
            _textField = textField;

            LoadResources();
        }

        private async void LoadResources()
        {
            _info = await _loadingServices.LoadByKey<CatalogAInfo>("AInfo");
            PrintInfo();
        }

        private void PrintInfo()
        {
            _textField.text = _info.Value;
        }
    }
}