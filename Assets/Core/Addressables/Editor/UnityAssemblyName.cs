﻿using System;

namespace AddressableEditor
{

    /// <summary>
    /// Класс для получения названия сборки из JSON
    /// </summary>
    [Serializable]
    public class UnityAssemblyName
    {
        /// <summary>
        /// Название сборки
        /// </summary>
        public string name;
    }
}