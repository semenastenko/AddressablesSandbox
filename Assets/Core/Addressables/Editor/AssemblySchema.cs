﻿using UnityEditor;
using UnityEditor.AddressableAssets.Settings;
using UnityEngine;

namespace AddressableEditor
{
    /// <summary>
    /// Scriptable Object схема для разделения ресурсов по сборкам
    /// </summary>
    public class AssemblySchema : AddressableAssetGroupSchema
    {
        /// <summary>
        /// Название сборки
        /// </summary>
        [SerializeField]
        private string _assemblyName;

        /// <summary>
        /// Папка сборки
        /// </summary>
        [SerializeField]
        private string _assemblyFolder;

        /// <summary>
        /// Булевая метка включения группы в билд
        /// Если true - группа включена в билд
        /// </summary>
        [SerializeField]
        private bool _includeInBuild;

        /// <summary>
        /// Булевая метка упакованной группы
        /// Если true - группа ресурсов не будет обновляться
        /// </summary>
        [SerializeField]
        private bool _isPacked;

        /// <summary>
        /// Название сборки
        /// </summary>
        public string AssemblyName
        {
            get => _assemblyName;
            internal set
            {
                if (_assemblyName != value)
                {
                    _assemblyName = value;
                    SetDirty(true);
                }
            }
        }

        /// <summary>
        /// Папка сборки
        /// </summary>
        public string AssemblyFolder
        {
            get => _assemblyFolder;
            internal set
            {
                if (_assemblyFolder != value)
                {
                    _assemblyFolder = value;
                    SetDirty(true);
                }
            }
        }

        /// <summary>
        /// Булевая метка включения группы в билд
        /// Если true - группа включена в билд
        /// </summary>
        public bool IncludeInBuild
        {
            get => _includeInBuild;
            internal set
            {
                if (_includeInBuild != value)
                {
                    _includeInBuild = value;
                    SetDirty(true);
                }
            }
        }

        /// <summary>
        /// Булевая метка упакованной группы
        /// Если true - группа ресурсов не будет обновляться
        /// </summary>
        public bool IsPacked
        {
            get => _isPacked;
            internal set
            {
                if (_isPacked != value)
                {
                    _isPacked = value;
                    SetDirty(true);
                }
            }
        }

        /// <summary>
        /// Сериализованный объект схемы
        /// </summary>
        private SerializedObject _serializedObject = null;

        /// <summary>
        /// Сериализованный объект схемы
        /// </summary>
        internal SerializedObject SerializedObject
        {
            get
            {
                if (_serializedObject == null)
                    _serializedObject = new SerializedObject(this);
                return _serializedObject;
            }
            set { _serializedObject = value; }
        }

        /// <summary>
        /// Метод обновления GUI инспектора
        /// </summary>
        public override void OnGUI()
        {
            ShowProperties(SerializedObject);
        }

        /// <summary>
        /// Метод отрисовки свойств объекта
        /// </summary>
        /// <param name="so">Сериализованный объект</param>
        void ShowProperties(SerializedObject so)
        {
            EditorGUI.BeginDisabledGroup(true);
            EditorGUILayout.PropertyField(so.FindProperty(nameof(_assemblyName)), true);
            EditorGUILayout.PropertyField(so.FindProperty(nameof(_assemblyFolder)), true);
            EditorGUILayout.PropertyField(so.FindProperty(nameof(_includeInBuild)), true);
            EditorGUI.EndDisabledGroup();
            EditorGUILayout.PropertyField(so.FindProperty(nameof(_isPacked)), true);
        }
    }
}