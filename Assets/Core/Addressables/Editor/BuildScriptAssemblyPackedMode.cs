﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using UnityEditor;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Build.DataBuilders;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.AddressableAssets.Settings.GroupSchemas;
using UnityEngine;
using Zenject;

namespace AddressableEditor
{
    /// <summary>
    /// Класс для распределения ресурсов по группам и сборки ресурсов в бандлы 
    /// </summary>
    [CreateAssetMenu(fileName = "BuildScriptAssemblyPackedMode", menuName = "Addressables/Custom Build/Assembly Packed")]
    public class BuildScriptAssemblyPackedMode : BuildScriptPackedMode
    {
        /// <summary>
        /// Название режима билдинга
        /// </summary>
        public override string Name => "Assembly Packed";

        /// <summary>
        /// Список сборок включенных в билд
        /// </summary>
        private HashSet<string> _inBuildAssemblies = new HashSet<string>();

        /// <summary>
        /// Метод построения данных 
        /// </summary>
        /// <typeparam name="TResult">Тип данных</typeparam>
        /// <param name="context">Контекст для построения данных</param>
        /// <returns>Результата построения данных</returns>
        protected override TResult BuildDataImplementation<TResult>(AddressablesDataBuilderInput context)
        {
            ResetGroups(context.AddressableSettings);
            CollectIncludeAssembliesInBuild();
            var result = base.BuildDataImplementation<TResult>(context);
            RelocateGroupAssets(context.AddressableSettings);
            ClearData();
            return result;
        }

        /// <summary>
        /// Метод обработки группы ресурсов
        /// </summary>
        /// <param name="assetGroup">Группа ресурсов</param>
        /// <param name="aaContext">Addressables контекст</param>
        /// <returns>Результат обработки ресурсов. Пустой если нет ошибки</returns>
        protected override string ProcessGroup(AddressableAssetGroup assetGroup, AddressableAssetsBuildContext aaContext)
        {
            if (assetGroup.HasSchema<AssemblySchema>())
            {
                var module = assetGroup.GetSchema<AssemblySchema>();
                if (_inBuildAssemblies.Contains(module.AssemblyName))
                {
                    module.IncludeInBuild = true;
                    SetIncludeInBuild(assetGroup, true);
                }
                else
                {
                    module.IncludeInBuild = false;
                    SetIncludeInBuild(assetGroup, false);
                }
            }
            else if (assetGroup.Default)
            {
                var errorString = AllocateResources(assetGroup, aaContext);
                if (!string.IsNullOrEmpty(errorString))
                    return errorString;

                SetIncludeInBuild(assetGroup, false);
            }

            return base.ProcessGroup(assetGroup, aaContext);
        }

        /// <summary>
        /// Метод назначения значения включения в билд для группы
        /// </summary>
        /// <param name="group">Группа ресурсов</param>
        /// <param name="value">значение включения в билд</param>
        private void SetIncludeInBuild(AddressableAssetGroup group, bool value)
        {
            if (group.HasSchema<BundledAssetGroupSchema>())
                group.GetSchema<BundledAssetGroupSchema>().IncludeInBuild = value;
        }

        /// <summary>
        /// Метод распределения ресурсов по группам
        /// </summary>
        /// <param name="group">Группа со всеми ресурсами</param>
        /// <param name="context">Addressables контекст</param>
        /// <returns>Результат распределения ресурсов. Пустой если нет ошибки</returns>
        private string AllocateResources(AddressableAssetGroup group, AddressableAssetsBuildContext context)
        {
            var entries = new List<AddressableAssetEntry>(group.entries);
            foreach (var entry in entries)
            {
                var result = AllocateResourceToGroup(entry, context);
                if (!string.IsNullOrEmpty(result))
                    return result;
            }

            return string.Empty;
        }

        /// <summary>
        /// Метод выделения ресурса в группу
        /// </summary>
        /// <param name="entry">Addressables ресурс</param>
        /// <param name="context">>Addressables контекст</param>
        /// <returns>Результат выделения ресурса. Пустой если нет ошибки</returns>
        private string AllocateResourceToGroup(AddressableAssetEntry entry, AddressableAssetsBuildContext context)
        {
            try
            {
                var root = entry.AssetPath.Split('/')[0];
                var directory = new FileInfo(entry.AssetPath).Directory;
                var assemblyPath = FindAssemblyPathInParentDirectory(root, directory);

                if (string.IsNullOrEmpty(assemblyPath))
                    return $"Can not find assembly path for {directory} in {root}";

                var assemblyName = GetAssemblyNameFromJSON(assemblyPath);

                if (string.IsNullOrEmpty(assemblyName))
                    return $"Can not get assembly name from JSON path {assemblyPath}";

                var assemblyDirectory = GetRelativeDirectory(new FileInfo(assemblyPath).Directory.FullName);
                var assemblyGroup = GetOrCreateAssemblyGroup(assemblyName, context.Settings, assemblyDirectory);
                context.Settings.MoveEntry(entry, assemblyGroup, false, false);
            }
            catch (Exception e)
            {

                Debug.Log($"{e.Message}");
                return e.Message;
            }
            return string.Empty;
        }

        /// <summary>
        /// Метод получения имени сборки из JSON
        /// </summary>
        /// <param name="assemblyPath">Путь к файлу сборки</param>
        /// <returns>Имя сборки</returns>
        private string GetAssemblyNameFromJSON(string assemblyPath)
        {
            var json = File.ReadAllText(assemblyPath);
            var assembly = JsonUtility.FromJson<UnityAssemblyName>(json);
            return assembly.name;
        }

        /// <summary>
        /// Метод получения относительного пути директории
        /// </summary>
        /// <param name="directory">Абсолютный путь директории</param>
        /// <returns>Относительный путь директории</returns>
        private string GetRelativeDirectory(string directory)
        {
            string dataPath = Application.dataPath;
            dataPath = dataPath.Replace("/Assets", "");
            dataPath = dataPath.Replace("/", "\\");
            var relative = directory.Replace(dataPath + "\\", "");
            return relative;
        }

        /// <summary>
        /// Метод получения или создания группы ресурсов сборки
        /// </summary>
        /// <param name="groupName">Название группы</param>
        /// <param name="settings">Настройки Addressables</param>
        /// <param name="assemblyDirectory">Относительный путь к директории сборки</param>
        /// <returns>Группа ресурсов сборки</returns>
        private AddressableAssetGroup GetOrCreateAssemblyGroup(string groupName, AddressableAssetSettings settings, string assemblyDirectory)
        {
            var group = settings.FindGroup(groupName);
            if (group == null)
            {
                group = settings.CreateGroup(groupName, false, false, false, null, typeof(BundledAssetGroupSchema), typeof(ContentUpdateGroupSchema), typeof(AssemblySchema));
                var module = group.GetSchema<AssemblySchema>();
                module.AssemblyName = groupName;
                module.AssemblyFolder = assemblyDirectory;
            }
            return group;
        }

        /// <summary>
        /// Метод сброса групп
        /// </summary>
        /// <param name="settings">Настройки Addressables</param>
        private void ResetGroups(AddressableAssetSettings settings)
        {
            var groups = new List<AddressableAssetGroup>(settings.groups);
            foreach (var group in groups)
            {
                if (group.HasSchema<AssemblySchema>() && !group.GetSchema<AssemblySchema>().IsPacked)
                {
                    settings.MoveEntries(new List<AddressableAssetEntry>(group.entries), settings.DefaultGroup, false, false);
                    settings.RemoveGroup(group);
                }
            }
        }

        /// <summary>
        /// Метод перемещения файлов группы
        /// </summary>
        /// <param name="settings">Настройки Addressables</param>
        private void RelocateGroupAssets(AddressableAssetSettings settings)
        {
            var groups = new List<AddressableAssetGroup>(settings.groups);
            foreach (var group in groups)
            {
                if (group.HasSchema<AssemblySchema>() && !group.GetSchema<AssemblySchema>().IsPacked)
                {
                    var module = group.GetSchema<AssemblySchema>();
                    var groupPath = module.AssemblyFolder + "\\AssetGroups";
                    MoveAsset(group, groupPath);

                    var schemas = new List<AddressableAssetGroupSchema>(group.Schemas);
                    foreach (var schema in schemas)
                    {
                        MoveAsset(schema, groupPath + "\\AssetSchemas");
                    }
                    AssetDatabase.Refresh();
                }
            }
        }

        /// <summary>
        /// Метод перемещения ресурса в новую папку
        /// </summary>
        /// <param name="asset">Ресурс</param>
        /// <param name="newPath">Путь к новой папке</param>
        private void MoveAsset(UnityEngine.Object asset, string newPath)
        {
            var assetPath = AssetDatabase.GetAssetPath(asset);
            var assetFile = new FileInfo(assetPath);

            if (!Directory.Exists(newPath))
                Directory.CreateDirectory(newPath);

            var groupModulePath = newPath + "\\" + assetFile.Name;

            AssetDatabase.MoveAsset(assetPath, groupModulePath);
        }

        /// <summary>
        /// Рекурсивный метод поиска пути к сборке в родительских директориях
        /// </summary>
        /// <param name="root">Название корневой папки</param>
        /// <param name="current">Текущая директория</param>
        /// <returns>Путь к сборке. Если пустой - не удалось найти сборку</returns>
        private string FindAssemblyPathInParentDirectory(string root, DirectoryInfo current)
        {
            var assemblyPath = string.Empty;
            foreach (var fileInfo in current.GetFiles())
            {
                if (fileInfo.Extension == ".asmdef")
                {
                    assemblyPath = fileInfo.FullName;
                }
            }

            if (string.IsNullOrEmpty(assemblyPath) && current.Name != root)
                return FindAssemblyPathInParentDirectory(root, current.Parent);
            else
                return assemblyPath;
        }

        /// <summary>
        /// Метод получения всех сборок включенных в билд
        /// </summary>
        private void CollectIncludeAssembliesInBuild()
        {
            var sceneContext = FindObjectOfType<SceneContext>();
            if (sceneContext != null)
            {
                var installers = sceneContext.Installers;
                foreach (var installer in installers)
                {
                    var assembly = Assembly.GetAssembly(installer.GetType());
                    _inBuildAssemblies.Add(assembly.GetName().Name);
                }
            }
        }

        /// <summary>
        /// Метод очистки данных
        /// </summary>
        private void ClearData()
        {
            _inBuildAssemblies.Clear();
        }
    }
}