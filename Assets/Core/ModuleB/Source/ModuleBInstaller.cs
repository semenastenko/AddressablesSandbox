using Sandbox.ModuleA;
using UnityEngine;
using Zenject;

namespace Sandbox.ModuleB
{
    public class ModuleBInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.Bind<ComplexSpawner>().AsSingle().NonLazy();
        }
    }
}