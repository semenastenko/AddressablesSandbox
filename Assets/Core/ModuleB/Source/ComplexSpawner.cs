using Sandbox.ModuleA;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Sandbox.ModuleB
{
    public class ComplexSpawner
    {
        private LoadingServices _loadingServices;

        private GameObject _prefab;

        public ComplexSpawner(LoadingServices loadingServices)
        {
            _loadingServices = loadingServices;
            LoadResources();
            Update();
        }

        private async void LoadResources()
        {
            _prefab = await _loadingServices.LoadByKey<GameObject>("ComplexShape");
        }

        private async void Update()
        {
            while (Application.isPlaying)
            {
                if (Input.GetKeyDown(KeyCode.D))
                {
                    Spawn();
                }

                await Task.Yield();
            }
        }

        private void Spawn()
        {
            Debug.Log($"Spawn B {_prefab}");
            if (_prefab != null)
            {
                Object.Instantiate(_prefab, Vector3.right * 5, Quaternion.identity);
            }
        }
    }
}
